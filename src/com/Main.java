package com;

import com.FileOutputRegistration;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.login.AdminLogin;
import com.login.UserLogin;
import com.services.Services;

public class Main {

	/**
	 * this main method consist of login for Admin and User New Registration for new
	 * User
	 * 
	 * @param args
	 * @throws IOException
	 * @author Batch-D
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("resource")
	public static void main(String args[]) throws IOException, ClassNotFoundException {

		// scanner initialization
		Scanner sc = new Scanner(System.in);

		// creating Object for Admin login
		AdminLogin admin = new AdminLogin();

		// creating the Object for Retrieve objects
		RetriveObjects retrive = new RetriveObjects();

		// creating the Object for User login
		UserLogin user_login = new UserLogin();

		// creating the object for Services
		Services services = new Services();

		// creating the object for FileoutputRegistration
		FileOutputRegistration fos = new FileOutputRegistration();

		// creating the new arraylist for admin login details
		ArrayList<String> admin_list = new ArrayList<String>();
		admin_list.add("admin");
		admin_list.add("admin123");
		admin_list.add("123456789012");

		// loop for choosing an option like Admin_Login or User_Login or
		// New_registration \
		// it will repeat untill true
		while (true) {

			System.out.println("1:Admin");
			System.out.println("2:User");
			System.out.println("3:New Customer Registration ");
			System.out.println("4:exit ");
			System.out.println("--------------------------");
			System.out.println("Enter the option :  ");

			// reads the input from the user
			String option = sc.next();

			// Switch case for user to choose the options: Admin_login or User_login or
			// New_User_Registration
			switch (option) {
			// case1: Option for Admin_Login
			case "1":
				// call admin_login method
				boolean flag = admin.adminLogin(admin_list);
				if (flag) {
					// calls aminServices for displaying the admin services
					services.adminServices();
				}
				break;

			// case2: Option for User_Login
			case "2":
				ArrayList<Register> temp_list = retrive.getListFromFile();
				String temp = user_login.userLogin(temp_list);

				services.userServices(temp);

				break;

			// case3: Option for New User Registration
			case "3": // calling validateID method
				String cus_id = fos.validateID();

				// calling the password method
				String pwd = fos.password();

				// calling the validateAdhar method
				String aadhar_no = fos.validaeAadhar();

				// passing all variables into the Register constructor
				com.Register r = new com.Register(cus_id, pwd, aadhar_no);

				// storing the data in the form of object into the file
				fos.storeObject(r);
				break;

			// case 4: for exit from the main page
			case "4":
				System.out.println("Thank you, visit again!");
				// System.exit(1);
				break;

			// default case for wrong option
			default:
				System.out.println("wrong option");
				break;
			}

		}

	}

}
