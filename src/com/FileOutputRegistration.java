package com;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
/**
 *class for creating the new  Registration 
 * @author Batch-D
 *
 */
public class FileOutputRegistration {
	
	/**
	 * Method for retrieving Register Object nad storing it into the file
	 * @param r
	 */
	public void storeObject(com.Register r) {

		OutputStream ops = null;
		ObjectOutputStream objOps = null;
		try {
			com.RetriveObjects retriveObjects = new com.RetriveObjects();
			List<com.Register> registerList = retriveObjects.getListFromFile();
			ops = new FileOutputStream("MyObjFile");
			objOps = new ObjectOutputStream(ops);
			// adding the Register object into the ArrayList
			registerList.add(r);

			// writing the arrayList
			objOps.writeObject(registerList);
			// System.out.println("you have registered successfully....");
			objOps.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (objOps != null)
					objOps.close();
			} catch (Exception ex) {

			}
		}
	}// Store object

	String cus_id;
	String pwd;
	String aadhar_num;

	// Initialzing the scanner
	Scanner sc = new Scanner(System.in);

	/**
	 * method to read the username and validate the username
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public String validateID() throws IOException, ClassNotFoundException {

		boolean flag = false;
		// reading the customer_id
		System.out.println("enter the Customer ID of 6 digits");
		cus_id = sc.next();

		File file = new File("MyObjFile");
		FileInputStream fis = new FileInputStream(file);
		@SuppressWarnings("resource")
		ObjectInputStream ois = new ObjectInputStream(fis);

		ArrayList<com.Register> list1 = new ArrayList<com.Register>();

		if (fis.available() > 0) {

			list1 = (ArrayList<com.Register>) ois.readObject();
		}

		// iterator for traversing
		Iterator<Register> it22 = list1.iterator();

		// for getting the elements one by one
		while (it22.hasNext() && !flag) {

			Register pair1 = (Register) it22.next();

			if (pair1.getCus_id().equals(cus_id)) {
				System.out.println("Customer Id Already exitsted");
				System.out.println();
				System.out.println("---Re-enter customer Id---");
				System.out.println();
				flag = true;
			
			}
		}
		if (flag) {
			// calling the ValidateId if the credentials wrong
			validateID();
		}
		// loop for validating the cus_id
		if ((cus_id.length()) != 6) {
			System.out.println("you have entered a wrong customer ID");
			validateID();
		} else {
			System.out.println("enter the password");
		}
		return cus_id;
	} // validate ID

	/**
	 * method to read the password
	 * 
	 * @return
	 */
	public String password() {
		return pwd = sc.next();
	}

	/**
	 * method for read aadhar from the user and validating the user
	 * 
	 * @return
	 */
	public String validaeAadhar() {
		System.out.println("please enter the aadhar number of 12-digits");
		aadhar_num = sc.next();

		if ((aadhar_num.length()) != 12) {
			System.out.println("please enter the 12-digit aadhar number");
			validaeAadhar();
		} else {
			Boolean numeric = true;
			try {
				@SuppressWarnings("unused")
				Double num = Double.parseDouble(aadhar_num);
			} catch (NumberFormatException e) {
				numeric = false;
			}
			if (!numeric) {
				System.out.println("type mismatch, enter the valid  aadhar");
				aadhar_num = sc.next();
			} else {
				System.out.println("registration done successfuly");
			}
		}
		return aadhar_num;
	}// End of validate aadhar

}// class
