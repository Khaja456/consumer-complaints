package com;

import java.io.Serializable;

/**
 * pojo class for new user registration with details like consumer_id, password,
 * and Aadhar number
 * 
 * @author Batch-D
 *
 */
public class Register implements Serializable {
	// variable for Serializing Id
	private static final long serialVersionUID = 1L;

	// class members declaration
	String cus_id;
	String pwd;
	String aadhar_num;

	// parameterized constructor for initializing class memebers
	public Register(String cus_id, String pwd, String aadhar_num) {
		super();
		this.cus_id = cus_id;
		this.pwd = pwd;
		this.aadhar_num = aadhar_num;
	}

	// getter and setter methods
	public String getCus_id() {
		return cus_id;
	}

	public void setCus_id(String cus_id) {
		this.cus_id = cus_id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getAadhar_num() {
		return aadhar_num;
	}

	public void setAadhar_num(String aadhar_num) {
		this.aadhar_num = aadhar_num;
	}

	// toString method to override the Hashcode.
	public String toString() {

		return new StringBuffer().append(this.cus_id)

				.append(" ").append(this.pwd).append("  ").append(this.aadhar_num).append("\n").toString();

	}
}
