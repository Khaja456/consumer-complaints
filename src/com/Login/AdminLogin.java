package com.login;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * class for admin login with Admin_id and password along with validations and
 * checking for incorrect credentials entered by the Admin if he reaches the
 * number of maximum attempts he can get his details back by entering the Aadhar
 * number
 * 
 * @author Batch-D
 *
 */
public class AdminLogin {

	// Initializing the scanner class
	Scanner sc = new Scanner(System.in);

	/**
	 * Method for Admin login and return boolean of ArrayList
	 */
	public boolean adminLogin(ArrayList<String> list2) throws IOException {

		boolean flag = false;
		int attempts = 0;

		// loop iterating until 3 attempts..
		attempts: while (attempts < 3) {
			// reading customer Id nad password from the user
			System.out.println("Enter Admin user ID :");
			String userName = sc.next();

			System.out.println("Enter user password :");
			String password = sc.next();

			// comparing the user details with registration details
			if (list2.get(0).equals(userName) && list2.get(1).equals(password)) {
				System.out.println(" ");
				System.out.println("Admin login successful....");
				System.out.println("-----*-----*-----*-----");
				System.out.println("  ");
				// if it is valid flat becomes true and atoping the iteration
				flag = true;
				break attempts;

			} else {
				// else incrementing the iteration
				attempts++;
				System.out.println("");
				System.out.println("you have entered the wrong username or password");
				System.out.println(" ");

				// if no of iterations equals to 3 then it will ask to enter the aadhar..
				if (attempts == 3) {

					System.out.println("You Reached Maximum attempts :");

					System.out.println("Enter your Aadhar Number");
					String aadhar = sc.next();

					// validating the aadhar, if OK then it will return user Id and password
					if (list2.get(2).equals(aadhar)) {

						System.out.println(
								"Your user Id is :" + " " + list2.get(0) + " " + "  Password is :" + list2.get(1));
						adminLogin(list2);
					} else {
						System.out.println("Aadhar details are invalid");
					}
				}
			}
		}

		return flag;
	}
}
