package com.login;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.Register;

/**
 * class for user login with user_id and password along with validations and
 * checking for incorrect credentials entered by the Admin if he reaches the
 * number of maximum attempts he can get his details back by entering the Aadhar
 * number
 * 
 * @author batch-D
 *
 */
public class UserLogin {

	// declaring the class members
	String user_name;
	String user_password;
	String aadhar;

	// method for validating user login....
	Scanner sc = new Scanner(System.in);

	/**
	 * Method for user login and return string of ArrayList
	 */
	public String userLogin(ArrayList<Register> list) {

		int attempts = 0;
		String temp = null;

		// loop will repeat untill 3 attempts
		// if id and password id OK then it will break the attempts
		attempts: while (attempts < 3) {

			System.out.println("Enter customer-ID : ");
			user_name = sc.next();

			System.out.println("Enter the password : ");
			user_password = sc.next();

			Iterator<Register> itr = list.iterator();

			// it will check weather next object is existed or not
			while (itr.hasNext()) {

				Register reg = (Register) itr.next();

				// it will validate the userId and password
				if (reg.getCus_id().equals(user_name)) {
					if ((reg.getPwd().equals(user_password))) {

						System.out.println("login successful...");
						temp = reg.getCus_id();
						break attempts;

					}
				} // uname

			} // itr while
			System.out.println(" ");
			// attempts incrementing
			attempts = attempts + 1;
			if (attempts < 3) {
				System.out.println("you have entered a wrong credentials... Try It again!");
			}

		} // attempts

		// if attempts == 3 it will ask the aadhar number
		// validate the aadhar, if it is OK, then it will return thr iser Id and
		// password
		if (attempts == 3) {
			System.out.println("you have reached maximum attempts....");
			System.out.println("----------------------");
			System.out.println("enter the aadhar number");
			aadhar = sc.next();

			Iterator<Register> itr1 = list.iterator();
			while (itr1.hasNext()) {
				Register reg1 = (Register) itr1.next();
				if (reg1.getAadhar_num().equals(aadhar)) {
					System.out.println(
							"Your user Id is :" + " " + reg1.getCus_id() + " " + "  Password is :" + reg1.getPwd());
					userLogin(list);

				}
			}
		}
		return temp;
	}

}
