package com.userlist;

import java.io.Serializable;

/**
 * POJO class for Complaint object it consist of consumer id, service name and complaint id 
 * @author Batch-D
 *
 */
public class Complaints implements Serializable{
	
	private static final long serialVersionUID = 1L;
	String consumer_id;
	String value;
	String complaint_id;
	public Complaints() {
		
	}
	//paramaterized constructor for compalint object
	public Complaints(String consumer_id, String value, String complaint_id) {
		super();
		this.consumer_id = consumer_id;
		this.value = value;
		this.complaint_id = complaint_id;
	}
	
	//getters and setters 
	public String getConsumer_id() {
		return consumer_id;
	}
	public void setConsumer_id(String consumer_id) {
		this.consumer_id = consumer_id;
	}
	//get the service name
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getComplaint_id() {
		return complaint_id;
	}
	public void setComplaint_id(String complaint_id) {
		this.complaint_id = complaint_id;
	}
	//overriding the hash code
	 public String toString() {
		  
	        return new StringBuffer().append(this.consumer_id)

	                .append(" ").append(this.value).append("  ").append(this.complaint_id
	                		).append("\n").toString();        
	    }
}
