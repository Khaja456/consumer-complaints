package com;

import java.io.*;
import java.util.ArrayList;

/**
 * class for retriveing the objects from the file and stroing it into the Array
 * list
 * 
 * @author IMVIZAG
 *
 */
public class RetriveObjects {

	@SuppressWarnings("unused")
	private com.Register r = null;

	@SuppressWarnings("unused")
	private InputStream fileIs;
	private ObjectInputStream objIs = null;

	/**
	 * method for reading the objects from the file
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<com.Register> getListFromFile() {

		// creating an empty list of Register type
		ArrayList<com.Register> registration_list = new ArrayList<com.Register>();
		try {
			File file = new File("MyObjFile");

			if (!file.exists()) {
				return new ArrayList<>();
			}
			FileInputStream fileIs = new FileInputStream(file);
			@SuppressWarnings("resource")
			ObjectInputStream objIs = new ObjectInputStream(fileIs);

			if (fileIs.available() > 0) {

				registration_list = (ArrayList<com.Register>) objIs.readObject();

			}

			return registration_list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (objIs != null)
					objIs.close();
			} catch (Exception ex) {

			}
		}
		return null;
	}
}// class