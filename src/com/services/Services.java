package com.services;

//import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.userlist.Complaints;

import java.util.Scanner;

/**
 * Services class provides the services for the customer for raising a complaint
 * and Admin for handling those user services.
 * 
 * @author Batch-D
 *
 */

public class Services {
	// initializing the scanner
	Scanner sc = new Scanner(System.in);
	UpdateService updateservice = new UpdateService();

//	public static final String userservice_file = "C:\\Users\\IMVIZAG\\eclipse-workspace\\consumercomplaints\\src\\com\\userservices.txt";
//	public static final String adminservices_file = "C:\\Users\\IMVIZAG\\eclipse-workspace\\consumercomplaints\\src\\com\\AdminServices.txt";
//	public static final String complaintslist_file2 = "C:\\Users\\IMVIZAG\\eclipse-workspace\\consumercomplaints\\src\\com\\complaintslist2.txt";
    
	public static final String userservice_file = "userservices";
	public static final String adminservices_file = "AdminServices";
	public static final String complaintslist_file2 = "complaintsList";
	
	// creating the Objects for Clases
	FileOperations fileOperations = new FileOperations();
	UpdateService updateService = new UpdateService();

	/**
	 * This method provides the services where user can select the service for
	 * raising a complaint
	 * 
	 * @throws ClassNotFoundException
	 */
	public void userServices(String temp) throws IOException, ClassNotFoundException {
		
		int random_num = (int)(Math.random() * 1000000 + 1);

		System.out.println("Enter the Selection  1:Complaint  2:Check Status  And 3:Update Password");
		
		String option = sc.nextLine();
		switch (option) {
		case "1":
			
//			FileOutputStream fos = new FileOutputStream(userservice_file);
//			ObjectOutputStream oos = new ObjectOutputStream(fos);
			// call deserialize method and storing the file data into the user_map1
			Map<Integer, String> user_services_map1 = fileOperations.deserialize(userservice_file);

			@SuppressWarnings("unused")

			int temp1 = user_services_map1.size();
			// if file does not contains any data then writing the data to the file
			if (user_services_map1.size() == 0) {

				// add services to the map
				user_services_map1.put(1, "Incorrect Bill");
				user_services_map1.put(2, "Irregular power supply");
				user_services_map1.put(3, "Low voltage problem");
				user_services_map1.put(4, "Issue with commercial connection");
				user_services_map1.put(5, "Payment issue");
				user_services_map1.put(6, "Illeagal power supply");
				user_services_map1.put(7, "Street light problem");
				user_services_map1.put(8, "Transformer issue");
				user_services_map1.put(9, "New connection problems");
				user_services_map1.put(10, "Upgrade to 3-phase");
                
				
				// storing the list into the file and deserialize the file
				fileOperations.serialize(user_services_map1, userservice_file);
			}
			
//			FileOutputStream fos = new FileOutputStream(userservice_file);
//			ObjectOutputStream oos = new ObjectOutputStream(fos);
			// Iterator for retriveing the file data into the map
			Iterator<Entry<Integer, String>> it = user_services_map1.entrySet().iterator();

			while (it.hasNext()) {
				
				@SuppressWarnings("rawtypes")
				Map.Entry pair = (Map.Entry) it.next();

				// printing the all services
				System.out.println(pair.getKey() + ". " + pair.getValue());
				it.remove();
			}

			System.out.println("Hey User Select a Service!...");
			int choose_service = Integer.parseInt(sc.nextLine());

			// retriveing the file data into the map by calling the deserialize method
			Map<Integer, String> user_map11 = fileOperations.deserialize(userservice_file);

			// Iterator for retriveing the file data into the map
			Iterator<Entry<Integer, String>> it1 = user_map11.entrySet().iterator();

			while (it1.hasNext()) {

				Map.Entry<Integer, String> pair = (Entry<Integer, String>) it1.next();

				if (pair.getKey() == choose_service) {
					
							System.out.println("you are Selected the Service : " + "  " + pair.getKey() + " : "
							+ pair.getValue() + ",   And Your reference id is " + random_num);

					// String consumer_id = temp;
					String service_name = pair.getValue();
					String complaint_id = Integer.toString(random_num);

					int complaint_key = 1000;
//                    
//					FileOutputStream fos1 = new FileOutputStream(complaintslist_file2);
//					ObjectOutputStream oos1 = new ObjectOutputStream(fos1);

					// retriveing the file data into the map by calling the deserialize method
					Map<Integer, Complaints> user_services_map2 = fileOperations
							.complaintsDeserialize(complaintslist_file2);

					// Iterator for retriveing the file data into the map
					Iterator<Entry<Integer, Complaints>> it22 = user_services_map2.entrySet().iterator();

					while (it22.hasNext()) {

						@SuppressWarnings("rawtypes")
						Map.Entry pair1 = (Map.Entry) it22.next();

						complaint_key = (int) pair1.getKey() + 1;
					}

					Complaints complaints = new Complaints();
					complaints.setConsumer_id(temp);
					complaints.setValue(service_name);
					complaints.setComplaint_id(complaint_id);

					user_services_map2.put(complaint_key, complaints);

					// storing the new data into the file
					fileOperations.complaintsSerialize(user_services_map2, complaintslist_file2);
				}
			}

			System.out.println("choose an option");
			System.out.println("1.To Select an Another Service");
			System.out.println("2.Logout");
			String user_choice = sc.nextLine();
			switch (user_choice) {
			case "1":
				this.userServices(temp);
				break;

			case "2":
			    System.out.println("------------");
			     break;
			case "3":
				updateservice.updatePassword(temp);
			     
			default :
				System.out.println("wrong Option");
			}
			break;

		case "2":
			updateservice.customerComplaintList(temp);
			System.out.println(" ");
			this.userServices(temp);
			break;
			
		case "3":
			updateservice.updatePassword(temp);
			System.out.println(" ");
			this.userServices(temp);
			break;
			
		default :
			System.out.println("wrong Option");
		}

	}// userService.

	/**
	 * adminServices method is providing the functionalities done by admin
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */

	public void adminServices() throws IOException, ClassNotFoundException {
		
         System.out.println("Admin Operations");
         System.out.println("----------------");
         
		// creating map object and adding the services to the map
		Map<Integer, String> admin_map = new HashMap<Integer, String>();
		
		admin_map.put(1, "Add service");
		admin_map.put(2, "Delete Service");
		admin_map.put(3, "Update Service");
		admin_map.put(4, "Check total complaints recevied");
		admin_map.put(5, "Check total number of customers");
		admin_map.put(6, "Update the Status of Complaint");
		admin_map.put(7, "Search by Customer Id or Complaint id");
		admin_map.put(8, "Remove the Existing Customer..!");
		admin_map.put(9, "LogOut..!");
       
		// storing the map data into the file
		fileOperations.serialize(admin_map, adminservices_file);

		// retriveing the file data into the map by calling the deserialize method
		Map<Integer, String> admin_map2 = fileOperations.deserialize(adminservices_file);

		// Iterator for retriveing the file data into the map
		Iterator<Entry<Integer, String>> it = admin_map2.entrySet().iterator();

		while (it.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + ". " + pair.getValue());
			it.remove();
		}

		// switch case for admin operations....
		boolean flag = true;
		while (flag) {
            System.out.println("");
			System.out.println("enter your choice to use admin services :");
		    String choice = sc.nextLine();
			switch (choice) {

			case "1":
				updateservice.showServices();
				addExtraService();
				break;

			case "2":
				updateservice.showServices();
				deleteservice();
				break;

			case "3":
				updateservice.showServices();
				updateService.updateService();
				break;

			case "4":
				updateService.checkTotalComplaints();
				break;

			case "5":
				updateService.displayTotalCustomers();
				break;
                                                 
			case "6":
				updateService.checkTotalComplaints1();
				updateService.upadteComplaintStatus();
				break;
				
			case "7" :
				updateService.search();
				break;
				
			case "8":
				updateService.removeCustomers();
				break;
				
			case "9":
				System.out.println("....Thank You....");
				System.out.println("");
				flag = false;
				System.exit(0);
				break;
			
			default:
				System.out.println("Invalid Option!...");
				break;

			}
		}

	}// admin_services

	/**
	 * method for delete the service
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void deleteservice() throws IOException, ClassNotFoundException {
        System.out.println("");
		System.out.println("enter the service to be delete!");
		int number = Integer.parseInt(sc.nextLine());

		// retriveing the file data into the map by calling the deserialize method  
		Map<Integer, String> user_map2 = fileOperations.deserialize(userservice_file);

		// removeing the service for the map
		user_map2.remove(number);

		System.out.println("service deleted And Size After the Deletion:" + user_map2.size());

		// storing the list into the file and deserialize the file
		fileOperations.serialize(user_map2, userservice_file);

		// Iterator for retriveing the file data into the map
		Iterator<Entry<Integer, String>> it = user_map2.entrySet().iterator();

		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + ". " + pair.getValue());
			it.remove();
		}
		// prints how many services are there in the file
		System.out.println(" services added to the file");
		System.out.println("choose an option 1.To Delete Service And 2.Go To Admin Services..!");
		String user_choice = sc.nextLine();
		switch (user_choice) {
		case "1":
			this.deleteservice();
			break;

		case "2":
			this.adminServices();
			break;
			
		default :
			System.out.println("wrong Option");
		}

	}// delete service
/**
 * method for adding the extra services to already existed services
 * @throws IOException
 * @throws ClassNotFoundException
 */
	public void addExtraService() throws IOException, ClassNotFoundException {
        System.out.println();
		System.out.println("enter the service to Add...");
		String extra = sc.nextLine();

		// retriveing the file data into the map by calling the deserialize method
		Map<Integer, String> user_map3 = fileOperations.deserialize(userservice_file);

		int size = user_map3.size();

		user_map3.put(size + 1, extra);
		// storing the new data into the file
		fileOperations.serialize(user_map3, userservice_file);

		// Iterator for retriveing the file data into the map
		Iterator<Entry<Integer, String>> it = user_map3.entrySet().iterator();

		while (it.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + ". " + pair.getValue());

		}

		// prints how many services are there in the file
		//System.out.println(user_map3.size());
		System.out.println("choose an option ");
		System.out.println();
		System.out.println(" 1.To Add an Extra Service...! ");
		System.out.println();
		System.out.println(" 2.Go To Admin Services..! ");
		System.out.println();
		String user_choice = sc.nextLine();
		switch (user_choice) {
		case "1":
			this.addExtraService();
			break;

		case "2":
			this.adminServices();
			break;
			
		default :
			System.out.println("wrong Option");
		}
	}
}