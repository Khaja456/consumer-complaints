package com.services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.userlist.Complaints;

/**
 * class for serialzing and deserialzing the data
 * @author Batch-D
 *
 */
public class FileOperations {

	/**
	 * method for deserilization 
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> deserialize(String file) throws IOException, ClassNotFoundException {

		FileInputStream fis = new FileInputStream(file);
		@SuppressWarnings("resource")
		
		ObjectInputStream ois = new ObjectInputStream(fis);

		Map<Integer, String> user_map = new HashMap<Integer, String>();

		if (fis.available() > 0) {

			user_map = (Map<Integer, String>) ois.readObject();
		}
		else {
			System.out.println("No Records are Found..!");
		}

		return user_map;
	}

	/**
	 * method for serialization
	 * @param user_map
	 * @param file
	 * @throws IOException
	 */
	public void serialize(Map<Integer, String> user_map, String file) throws IOException {

		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(user_map);
		fos.close();
		oos.close();

	}
	/**
	 * Method for deserializing the data in the file, and storing it into the HashMap
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer,Complaints> complaintsDeserialize(String file) throws IOException, ClassNotFoundException{

		FileInputStream fis = new FileInputStream(file);
		@SuppressWarnings("resource")
		ObjectInputStream ois = new ObjectInputStream(fis);

		Map<Integer,Complaints > user_map = new HashMap<Integer, Complaints>();
		

		if (fis.available() > 0) {

			user_map = (Map<Integer, Complaints>) ois.readObject();
		}
		else {
			System.out.println("No Compalints Found..!");
		}

		return user_map;
	}
	
	/**
	 * method for serialzing the complaint the data into the file
	 * @param user_map
	 * @param file
	 * @throws IOException
	 */
	public void complaintsSerialize(Map<Integer, Complaints> user_map, String file) throws IOException {

		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(user_map);
		fos.close();
		oos.close();
	}
}
