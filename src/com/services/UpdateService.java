package com.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import com.Register;
import com.userlist.Complaints;

/**
 * class for updating the service with new value
 * @author LENOVO
 *
 */
public class UpdateService {

//	public static final String userservice_file = "C:\\Users\\IMVIZAG\\eclipse-workspace\\consumercomplaints\\src\\com\\userservices.txt";
//
//	public static final String complaintslist_file2 = "C:\\Users\\IMVIZAG\\eclipse-workspace\\consumercomplaints\\src\\com\\complaintslist2.txt";
	
	
	public static final String userservice_file = "userservices";

	public static final String complaintslist_file2 = "complaintsList";
	Scanner sc = new Scanner(System.in);

	FileOperations fileOperations = new FileOperations();

	/**
	 * method for updating the existed service with new value
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void updateService() throws IOException, ClassNotFoundException {

		// retriveing the file data into the map by calling the deserialize method
		Map<Integer, String> add_Service = fileOperations.deserialize(userservice_file);

		System.out.println("enter which service has to be updated");

		// reading which service has to be updated
		int update = Integer.parseInt(sc.nextLine());

		// if map contains the key hen it will update the service
		if (add_Service.containsKey(update)) {

			System.out.println("enter the service");
			String add_service = sc.nextLine();

			// add_Service.put(update,add_service);
			String old_Service = add_Service.put(update, add_service);
			assert (old_Service == null);
			System.out.println("service added sucessfully..");
		}

		//storing the new data into the file
		fileOperations.serialize(add_Service, userservice_file);
		
		System.out.println("choose an option 1.To Update an Extra Service And 2.Go To Admin Services..!");
		int user_choice = sc.nextInt();
		switch(user_choice) {
		case 1:this.updateService();
		       break;
		       
		case 2:new Services().adminServices();
		       break;
		}
	}
/**
 * method for checking the toatal number of complaints received
 * @throws IOException
 * @throws ClassNotFoundException
 */
	public void checkTotalComplaints() throws IOException, ClassNotFoundException {

		// retriveing the file data into the map by calling the deserialize method
		Map<Integer, String> add_Service = fileOperations.deserialize(complaintslist_file2);

		
		Iterator<Entry<Integer, String>> it22 = add_Service.entrySet().iterator();

		while (it22.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry pair1 = (Map.Entry) it22.next();

			System.out.println(pair1.getKey() + "." + pair1.getValue());
			it22.remove();
		}
		System.out.println("Redirecting to Admin Services...!");
		System.out.println();
		new Services().adminServices();
	}

	/**
	 * method for displaying the total number of customers
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public void displayTotalCustomers() throws IOException, ClassNotFoundException {
       
		//creatinng file object ant storing the MyObjFile int othe variable
		File file = new File("MyObjFile");
		//streams for deserializing the data in the file
		FileInputStream fis = new FileInputStream(file);
		@SuppressWarnings("resource")
		ObjectInputStream ois = new ObjectInputStream(fis);

		//creating ArrayList of the type Register
		ArrayList<com.Register> list1 = new ArrayList<com.Register>();

		if (fis.available() > 0) {

			list1 = (ArrayList<com.Register>) ois.readObject();
		}

		Iterator<Register> it22 = list1.iterator();

		while (it22.hasNext()) {

			Register pair1 = (Register) it22.next();

			System.out.println("Consumer ID:  " + pair1.getCus_id() + "   " + "Password:  " + pair1.getPwd()   + "   "
					+ "Aadhar Number:  " + pair1.getAadhar_num());
			it22.remove();
		}
		
		System.out.println("Redirecting to Admin Services...!");
		new Services().adminServices();
	}
	/**
	 * In this method Admin can update the users complaint status
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void upadteComplaintStatus() throws ClassNotFoundException, IOException {
		
				// retriveing the file data into the map by calling the deserialize method
				Map<Integer, Complaints> compalints_map = fileOperations.complaintsDeserialize(complaintslist_file2);
				
				System.out.println("enter the Key Number to Update the service..!");
				int key_value = Integer.parseInt(sc.nextLine());
				
				if(compalints_map.containsKey(key_value)) {
					Complaints update = compalints_map.get(key_value);
					
					System.out.println("Update the status of the Compalint..!");
					String temp = sc.nextLine();
					update.setValue(temp);
					compalints_map.put(key_value, update);
					
					fileOperations.complaintsSerialize(compalints_map, complaintslist_file2);
				}
				else {
					System.out.println("please Enter a valid Key..!");
				}
				System.out.println("Redirecting to Admin Services...!");
				System.out.println();
				new Services().adminServices();
	}
	/**
	 * In this method customer can see the status of the respective complaints
	 * @param temp
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void customerComplaintList(String temp) throws ClassNotFoundException, IOException {
	
		// retriveing the file data into the map by calling the deserialize method
		Map<Integer, Complaints> compalints_map = fileOperations.complaintsDeserialize(complaintslist_file2);
		
		@SuppressWarnings("unused")
		Iterator<Entry<Integer, Complaints>> it22 = compalints_map.entrySet().iterator();

		for(Map.Entry<Integer, Complaints> entry :compalints_map.entrySet()) {
			Complaints update = entry.getValue();
			if(update.getConsumer_id().equals(temp)) {
				System.out.println("Here is your complaints status.....");
				System.out.println(update.getValue());
				System.out.println("your complaint ID is :  "+update.getComplaint_id());
				System.out.println("");
			} 
		}
	}
	
	/**
	 * method for showing all the services to the user
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void showServices() throws ClassNotFoundException, IOException {
		
		Map<Integer, String> user_map12 = fileOperations.deserialize(userservice_file);
		
		// Iterator for retriveing the file data into the map
		Iterator<Entry<Integer, String>> it12 = user_map12.entrySet().iterator();

		while (it12.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it12.next();

			// printing the all services
			System.out.println(pair.getKey() + ". " + pair.getValue());
			it12.remove();
		}
	}
	/**
	 * method for serach the customer ID or Complaint ID  of the complaint
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void search() throws ClassNotFoundException, IOException {
		
		System.out.println("enter the customer Id or Complaint Id to search complaint...!");
		String key_value = sc.next();

		// retriveing the file data into the map by calling the deserialize method
		Map<Integer, Complaints> compalints_map = fileOperations.complaintsDeserialize(complaintslist_file2);
		
		@SuppressWarnings("unused")
		Iterator<Entry<Integer, Complaints>> it22 = compalints_map.entrySet().iterator();
        boolean flag = false;
		for(Map.Entry<Integer, Complaints> entry :compalints_map.entrySet()) {
			
			Complaints update = entry.getValue();
			if(update.getConsumer_id().equals(key_value) || update.getComplaint_id().equals(key_value)) {
				
				System.out.println("Here is your complaints status.....");
				System.out.println("");
				System.out.println(update.getValue()+ "   and your complaint ID is :  "+update.getComplaint_id());
				flag = true;
			} 
			
		}
		if(!flag) {
			System.out.println("---There is no  such ID found!---");
		}
	}
	public void checkTotalComplaints1() throws IOException, ClassNotFoundException {

		// retriveing the file data into the map by calling the deserialize method
		Map<Integer, String> add_Service = fileOperations.deserialize(complaintslist_file2);

		
		Iterator<Entry<Integer, String>> it22 = add_Service.entrySet().iterator();

		while (it22.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry pair1 = (Map.Entry) it22.next();

			System.out.println(pair1.getKey() + "." + pair1.getValue());
			it22.remove();
		}
	}
	
	@SuppressWarnings("unchecked")
	
	public void removeCustomers() throws IOException, ClassNotFoundException {
		 
		//creatinng file object ant storing the MyObjFile int othe variable
		File file = new File("MyObjFile");
		//streams for deserializing the data in the file
		FileInputStream fis = new FileInputStream(file);
		@SuppressWarnings("resource")
		ObjectInputStream ois = new ObjectInputStream(fis);
        
		System.out.println("Enter the User Id to delete..!");
		String userId = sc.nextLine();
		
		//creating ArrayList of the type Register
		ArrayList<com.Register> list1 = new ArrayList<com.Register>();

		if (fis.available() > 0) {

			list1 = (ArrayList<com.Register>) ois.readObject();
		}
        boolean flag = true;
		Iterator<Register> it22 = list1.iterator();

		while (it22.hasNext() && flag) {

			Register pair1 = (Register) it22.next();

//			System.out.println("Consumer ID:  " + pair1.getCus_id() + "   " + "Password:  " + pair1.getPwd()   + "   "
//					+ "Aadhar Number:  " + pair1.getAadhar_num());
//			it22.remove();
			if(pair1.getCus_id().equals(userId)) {
				list1.remove((Register)pair1);
				System.out.println("removed User SuccesFully");
				flag = false;
			}
		}
		
		if(flag) {
			System.out.println("No such Customer Id Found in Database..");
			System.out.println("");
		}
		else {
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(list1);
	 		fos.close();
			oos.close();
		}
		
		System.out.println("Redirecting to Admin Services...!");
		new Services().adminServices();
	}
	
	@SuppressWarnings("unchecked")
	public void updatePassword(String temp) throws IOException, ClassNotFoundException {
		
		System.out.println("Enter the Old Password..!");
		String oldPassword = sc.next();
		
		System.out.println("Enter the New Password...!");
		String newPassword = sc.next();
		
		//creatinng file object ant storing the MyObjFile int othe variable
				File file = new File("MyObjFile");
				//streams for deserializing the data in the file
				
				FileInputStream fis = new FileInputStream(file);
				
				@SuppressWarnings("resource")
				ObjectInputStream ois = new ObjectInputStream(fis);

				//creating ArrayList of the type Register
				ArrayList<com.Register> list1 = new ArrayList<com.Register>();

				if (fis.available() > 0) {

					list1 = (ArrayList<com.Register>) ois.readObject();
				}

				Iterator<Register> it22 = list1.iterator();

				while (it22.hasNext()) {

					Register pair1 = (Register) it22.next();

					if(pair1.getPwd().equals(oldPassword)) {
						pair1.setPwd(newPassword);
						System.out.println("Updated Succesfully..!");
						
						FileOutputStream fos = new FileOutputStream(file);
						ObjectOutputStream oos = new ObjectOutputStream(fos);
						oos.writeObject(list1);
				 		fos.close();
						oos.close();
					}
					it22.remove();
				}
	}
}
